// cbcli is a command line tool to generate keys , create signatures and verify them
package main

import (
	"crypto/rand"
	"encoding/gob"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"bitbucket.org/rmanolis/cblib"
	"github.com/urfave/cli"
)

func main() {
	gob.Register(cblib.Signature{})
	app := cli.NewApp()
	app.Name = "cbcli"
	app.Usage = "Create and verify digital signatures"
	app.Commands = []cli.Command{generate, verify, sign, encrypt, decrypt}

	app.Run(os.Args)
}

var encrypt = cli.Command{
	Name:    "encrypt",
	Aliases: []string{"e"},
	Usage:   "--public=<FILENAME>  --private=<FILENAME, optional> --file=<FILENAME> --output=<FILENAME, optional>",
	Flags: []cli.Flag{
		cli.StringFlag{
			Name:  "public",
			Usage: "The public key of the receiver.",
		},
		cli.StringFlag{
			Name:  "private",
			Usage: "Your private key.",
			Value: "default_key.cbpriv",
		},
		cli.StringFlag{
			Name:  "file",
			Usage: "The file for encryption.",
		},
		cli.StringFlag{
			Name:  "output",
			Usage: "The encrypted file.",
			Value: "",
		},
	},
	Action: func(c *cli.Context) error {
		public := c.String("public")
		private := c.String("private")
		fileName := c.String("file")
		output := c.String("output")

		pub, err := ioutil.ReadFile(public)
		if err != nil {
			fmt.Println("Error:", err)
			return err
		}

		priv, err := ioutil.ReadFile(private)
		if err != nil {
			fmt.Println("Error:", err)
			return err
		}

		file, err := os.Open(fileName)
		if err != nil {
			fmt.Println("Error:", err)
			return err
		}
		b, err := ioutil.ReadAll(file)
		if err != nil {
			fmt.Println("Error:", err)
			return err
		}

		v, err := cblib.Encrypt(priv, pub, b)
		if err != nil {
			fmt.Println("Error:", err)
			return err
		}

		if len(output) == 0 {
			output = file.Name() + ".cbenc"
		} else {
			output += ".cbenc"
		}

		err = ioutil.WriteFile(output, v, 0644)
		if err != nil {
			fmt.Println("Error:", err)
			return err
		}
		return nil
	},
}

var decrypt = cli.Command{
	Name:    "decrypt",
	Aliases: []string{"d"},
	Usage:   "--public=<FILENAME>  --private=<FILENAME, optional> --file=<FILENAME> --output=<FILENAME,optional>",
	Flags: []cli.Flag{
		cli.StringFlag{
			Name:  "public",
			Usage: "The public key of the sender.",
		},
		cli.StringFlag{
			Name:  "private",
			Usage: "Your private key.",
			Value: "default_key.cbpriv",
		},
		cli.StringFlag{
			Name:  "file",
			Usage: "The file for decryption.",
		},
		cli.StringFlag{
			Name:  "output",
			Usage: "The decrypted file.",
			Value: "",
		},
	},
	Action: func(c *cli.Context) error {
		fileName := c.String("file")
		output := c.String("output")
		priv, err := ioutil.ReadFile(c.String("private"))
		if err != nil {
			return err
		}

		pub, err := ioutil.ReadFile(c.String("public"))
		if err != nil {
			return err
		}

		file, err := os.Open(fileName)
		if err != nil {
			return err
		}
		b, err := ioutil.ReadAll(file)
		if err != nil {
			return err
		}

		v, err := cblib.Decrypt(priv, pub, b)
		if err != nil {
			return err
		}

		if len(output) == 0 {
			output = strings.Replace(file.Name(), ".cbenc", "", 1)
		}
		err = ioutil.WriteFile(output, v, 0644)
		if err != nil {
			return err
		}
		return nil
	},
}

var generate = cli.Command{
	Name:    "generate",
	Aliases: []string{"g"},
	Usage:   "--key=<FILENAME,optional> --curve=<P256|P384|P512>",
	Flags: []cli.Flag{
		cli.StringFlag{
			Name:  "key",
			Value: "default_key",
		},
		cli.StringFlag{
			Name:  "curve",
			Value: "P256",
		},
	},
	Action: func(c *cli.Context) error {
		curve_name := c.String("curve")
		keys, err := cblib.GenerateKeys(curve_name, rand.Reader)
		if err != nil {
			return err
		}
		ioutil.WriteFile(c.String("key")+".cbpriv", keys.PrivateKey, 0644)
		ioutil.WriteFile(c.String("key")+".cbpub", keys.PublicKey, 0644)
		return nil
	},
}

var verify = cli.Command{
	Name:    "verify",
	Aliases: []string{"v"},
	Usage:   "--public=<FILENAME,optional> --signature=<FILENAME> --file=<FILENAME> ",
	Flags: []cli.Flag{
		cli.StringFlag{
			Name:  "public",
			Value: "default_key.cbpub",
		},
		cli.StringFlag{
			Name:  "signature",
			Value: "default_signature.cbsign",
		},
		cli.StringFlag{
			Name: "file",
		},
	},
	Action: func(c *cli.Context) error {
		pub, err := ioutil.ReadFile(c.String("public"))
		if err != nil {
			return err
		}
		file, err := ioutil.ReadFile(c.String("file"))
		if err != nil {
			return err
		}
		signature, err := ioutil.ReadFile(c.String("signature"))
		if err != nil {
			return err
		}

		v, err := cblib.Verify(pub, file, signature)
		if err != nil {
			return err
		}

		fmt.Println(v)
		return nil
	},
}

var sign = cli.Command{
	Name:    "sign",
	Aliases: []string{"s"},
	Usage:   "--private=<FILENAME,optional> --file=<FILENAME> --signature=<FILENAME,optional>",
	Flags: []cli.Flag{
		cli.StringFlag{
			Name:  "private",
			Value: "default_key.cbpriv",
		},
		cli.StringFlag{
			Name: "file",
		},
		cli.StringFlag{
			Name:  "signature",
			Value: "default_signature.cbsign",
		},
	},
	Action: func(c *cli.Context) error {
		private, err := ioutil.ReadFile(c.String("private"))
		if err != nil {
			return err
		}
		file, err := ioutil.ReadFile(c.String("file"))
		if err != nil {
			return err
		}
		sign, err := cblib.Sign(file, private, rand.Reader)
		if err != nil {
			return err
		}
		ioutil.WriteFile(c.String("signature"), sign, 0644)
		return nil
	},
}
